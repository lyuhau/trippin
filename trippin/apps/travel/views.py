from django.shortcuts import render, redirect
from apps.login.models import User
from apps.travel.models import Trip

def index(request):
    if not request.session['userId']:
        return redirect('/')

    user = User.objects.get(userId=request.session['userId'])
    data = {
        'username': user.username,
        'tripsPlanned': user.tripsPlanned.all(),
        'tripsJoined': user.tripsJoined.all(),
        'trips': Trip.objects.getPublicTrips(request.session['userId'])
    }
    return render(request, 'travel/index.html', data)

def createForm(request):
    return render(request, 'travel/create.html')

def create(request):
    Trip.objects.createTrip(
        request.session['userId'],
        request.POST['destination'],
        request.POST['description'],
        request.POST['startDate'],
        request.POST['endDate'],
        )
    return redirect('/travel')

def delete(request):
    Trip.objects.get(tripId=request.POST['tripId']).delete()
    return redirect('/travel')

def join(request):
    Trip.objects.join(
        tripId=request.POST['tripId'],
        userId=request.session['userId'],
    )
    return redirect('/travel')

def leave(request):
    Trip.objects.leave(
        tripId=request.POST['tripId'],
        userId=request.session['userId'],
    )
    return redirect('/travel')
