from django.conf.urls import url, include
from apps.travel import views

urlpatterns = [
    url(r'^travel$', views.index),
    url(r'^travel/createForm$', views.createForm),
    url(r'^travel/create$', views.create),
    url(r'^travel/delete$', views.delete),
    url(r'^travel/join$', views.join),
    url(r'^travel/leave$', views.leave),
]
