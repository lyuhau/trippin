from django.db import models
from django.db.models import Q
from apps.login.models import User

class TripManager(models.Manager):
    def createTrip(self, userId, destination, description, startDate, endDate):
        created = Trip.objects.create(plannedBy=User.objects.get(userId=userId), destination=destination, description=description, startDate=startDate, endDate=endDate)
        return (201, None, created)

    def getPublicTrips(self, userId):
        return Trip.objects.filter(~Q(plannedBy=User.objects.get(userId=userId)))

    def join(self, tripId, userId):
        Trip.objects.get(tripId=tripId).usersJoined.add(User.objects.get(userId=userId))

    def leave(self, tripId, userId):
        Trip.objects.get(tripId=tripId).usersJoined.remove(User.objects.get(userId=userId))

class Trip(models.Model):
    tripId = models.IntegerField(primary_key=True)
    plannedBy = models.ForeignKey(User, on_delete='CASCADE', related_name='tripsPlanned')
    destination = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    startDate = models.DateField()
    endDate = models.DateField()
    usersJoined = models.ManyToManyField(User, related_name='tripsJoined')
    createAt = models.DateTimeField(auto_now_add=True)
    updateAt = models.DateTimeField(auto_now=True)
    objects = TripManager()
