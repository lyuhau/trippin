from django.db import models, IntegrityError

class UserManager(models.Manager):
    def register(self, username, password):
        try:
            created = User.objects.create(username=username, password=password)
            return (201, None, created)
        except IntegrityError:
            return (400, 'Username {} already in use'.format(username), None)

    def login(self, username, password):
        matchingUsers = User.objects.filter(username=username, password=password)
        if len(matchingUsers) == 1:
            return (200, None, matchingUsers[0])
        else:
            return (404, 'No user found with username {} and given password'.format(username), None)

class User(models.Model):
    userId = models.IntegerField(primary_key=True)
    username = models.CharField(max_length=255, unique=True)
    password = models.CharField(max_length=255)
    createAt = models.DateTimeField(auto_now_add=True)
    updateAt = models.DateTimeField(auto_now=True)
    objects = UserManager()
