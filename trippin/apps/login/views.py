from django.shortcuts import render, redirect
from apps.login.models import User

def index(request):
    return render(request, 'login/index.html')

def login(request):
    (status, message, user) = User.objects.login(request.POST['username'], request.POST['password'])
    data = { 'message': None }
    if status == 200:
        request.session['userId'] = user.userId
        data['message'] = 'Logged in as {}!'.format(user.username)
        return redirect('/travel')
    elif status == 404:
        data['message'] = message
        return render(request, 'login/index.html', data)

def register(request):
    (status, message, user) = User.objects.register(request.POST['username'], request.POST['password'])
    data = { 'message': None }
    if status == 201:
        data['message'] = 'User {} successfully created!'.format(user.username)
    elif status == 400:
        data['message'] = message

    return render(request, 'login/index.html', data)

def logout(request):
    request.session['userId'] = None
    return redirect('/')
